
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * 4. Crie um método que recebe um array de inteiros positivos e substitui seus
 * elementos de valor ímpar por ‐1 e os pares por +1.
 */
public class Ex4 {

    public static void main(String[] args) {

        int tamanho = 10;
        //definindo array de tamanho 80
        int meuArray[] = new int[tamanho];
        //criando scanner para preencher o array
        Scanner scanner = new Scanner(System.in);

        //preenchendo o array 
        for (int i = 0; i < meuArray.length; i++) {
            System.out.print("Digite o elemento[" + i + "]:");
            meuArray[i] = scanner.nextInt();
        }

        imprimirArray(meuArray);
        
        System.out.println("");
        
        substituirElementos(meuArray);
        imprimirArray(meuArray);

    }

    public static void imprimirArray(int array[]) {

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);

            //verificando se esta imprimindo o ultimo elemento
            if (array.length == i + 1) {
                System.out.print(" .");
            } else { // ainda existe elementos colocar vigurla 
                System.out.print(" , ");
            }

        }

    }

    private static void substituirElementos(int[] meuArray) {

        for (int i = 0; i < meuArray.length; i++) {
            if (meuArray[i] % 2 == 0) {
                meuArray[i] = 1;
            } else {
                meuArray[i] = -1;
            }
        }

    }

}
