
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * 5. Escreva um programa que leia as notas de N alunos . Após a leitura das
 * notas calcule a media da turma e a imprima. Imprima também somente as notas
 * acima da media da turma.
 */
public class Ex5 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int quantidade;
        System.out.print("Digite quantas notas:");
        quantidade = scanner.nextInt();
        int alunos[] = new int[quantidade];

        for (int i = 0; i < alunos.length; i++) {
            System.out.print("Digite a nota do aluno" + i);
            alunos[i] = scanner.nextInt();
        }

        double media = calcularMedia(alunos);

        System.out.println("Media da turma:" + media);

        for (int i = 0; i < alunos.length; i++) {
            if (alunos[i] > media) {
                System.out.println("Aluno" + i);
            }
        }

    }

    private static double calcularMedia(int[] alunos) {
        double media = 0;
        double soma = 0;

        for (int i = 0; i < alunos.length; i++) {
            soma += alunos[i];
        }
        media = soma / alunos.length;
        return media;
    }

   

}
