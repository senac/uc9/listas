
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * 1. Crie um array de inteiros e de tamanho 8 e o preencha . Crie um método que imprima
      todo o array em linha separado por virgula os elementos.
 */
public class Ex1 {
    
    public static void main(String[] args) {
        
        //definindo array de tamanho 8
        int meuArray[] = new int[8] ; 
        //criando scanner para preencher o array
        Scanner scanner = new Scanner(System.in);
        
        //preenchendo o array 
        for(int i= 0 ; i < meuArray.length ; i++){
            System.out.print("Digite o elemento[" + i + "]:");
            meuArray[i] = scanner.nextInt() ; 
        }
        
        imprimirArray(meuArray);
        
        
    }
    
    public static void imprimirArray(int array[]){
        
        for(int i = 0 ; i < array.length ; i++){
            System.out.print(array[i]);
           
            //verificando se esta imprimindo o ultimo elemento
            if(array.length == i + 1 ){
                System.out.print(" .");
            }else{ // ainda existe elementos colocar vigurla 
                System.out.print(" , ");
            }
            
        }
        
    }
    
}
