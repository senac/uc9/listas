
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * 2. Crie um array de inteiros e de tamanho 80 e o preencha . Crie um método
 * que retorne se um dado elemento existe no array .
 */
public class Ex2 {

    public static void main(String[] args) {

        int tamanho = 80;
        //definindo array de tamanho 80
        int meuArray[] = new int[tamanho];
        //criando scanner para preencher o array
        Scanner scanner = new Scanner(System.in);

        //preenchendo o array 
        for (int i = 0; i < meuArray.length; i++) {
            System.out.print("Digite o elemento[" + i + "]:");
            meuArray[i] = scanner.nextInt();
        }

        System.out.print("Digite o elemento que busca:");
        int elemento = scanner.nextInt();

        if (existeElementoNoArray(elemento, meuArray)) {
            System.out.println("Elemento existe no array");
        } else {
            System.out.println("Elemento nao existe no array");
        }

    }

    private static boolean existeElementoNoArray(int e, int[] array) {

        boolean achou = false;

        for (int i = 0; i < array.length; i++) {
            if (e == array[i]) {
                achou = true;
                break;
            }
        }

        return achou;
    }

}
