
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * 6. Crie um array de inteiros de tamanho 10 e os preencha com quaisquer
 * valores utilizando loop. Peça para o usuário informar um valor qualquer. Crie
 * um método que receba como argumento um array e um valor e retorne o numero de
 * ocorrências do elemento no array.
 */
public class Ex6 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int tamanho = 10;
        int meuArray[] = new int[tamanho];

        //preenchendo o array 
        for (int i = 0; i < meuArray.length; i++) {
            System.out.print("Digite numero:");
            meuArray[i] = scanner.nextInt();
        }

        System.out.println("Digite o valor:");
        int numero = scanner.nextInt();
        int quantidadeDeOcorrencias = buscaQuantidadeOcorrencia(numero, meuArray);

        System.out.println(String.format("Existem %d ocorrencias no array", quantidadeDeOcorrencias));

    }

    private static int buscaQuantidadeOcorrencia(int numero, int[] meuArray) {

        int ocorrencias = 0;

        for (int i = 0; i < meuArray.length; i++) {
            if (meuArray[i] == numero) {
                ocorrencias++;
            }
        }

        return ocorrencias;
    }

}
