
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * 3. Escreva um método que recebe um array de números e devolve a posição onde
 * se encontra o maior valor do array. Se houver mais de um valor maior,
 * devolver a posição da primeira ocorrência.
 */
public class Ex3 {

    public static void main(String[] args) {

        int tamanho = 10;
        //definindo array de tamanho 80
        int meuArray[] = new int[tamanho];
        //criando scanner para preencher o array
        Scanner scanner = new Scanner(System.in);

        //preenchendo o array 
        for (int i = 0; i < meuArray.length; i++) {
            System.out.print("Digite o elemento[" + i + "]:");
            meuArray[i] = scanner.nextInt();
        }

        int posicaoMaior = buscaPosicaoMaior(meuArray);
        String saida = String.format("O maior elemento (%d) se encontra na posicao %d", meuArray[posicaoMaior], posicaoMaior);
        System.out.println(saida);

    }

    private static int buscaPosicaoMaior(int[] array) {

        // ate se conhecer os proximos elementos o primeiro valor é o maior .
        int maior = array[0];

        //fazendo comparações com os outros elementos do array
        for (int i = 0; i < array.length; i++) {
            if (array[i] > array[maior]) {
                maior = i;
            }
        }

        return maior;
    }

}
