
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author sala308b
 */
public class ArrayUtil {

    public static void preencher(int[] n) {

        for (int i = 0; i < n.length; i++) {
            n[i] = Integer.parseInt(JOptionPane.showInputDialog("Digite Valor : "));
        }
    }

    public static void imprimir(int[] n) {
        for (int i = 0; i < n.length; i++) {
            System.out.println(n[i] + " , ");
        }
    }

    public static void preencher(int[][] m) {

        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                m[i][j] = Integer.parseInt(JOptionPane.
                        showInputDialog("Digite[" + i + "][" + j + "]"));
            }
        }

    }

    public static void imprimir(int[][] m) {
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                System.out.print(m[i][j] + " ");
            }

            System.out.print("\n");
        }
    }

    public static double calcularMedia(double[] notas) {
        return somaElementos(notas) / notas.length;
    }

    public static double somaElementos(double[] notas) {

        double soma = 0;

        for (int i = 0; i < notas.length; i++) {
            soma += notas[i];
        }

        return soma;

    }

    public static void imprimeElementosAcimaValor(double[] notas, double valor) {
        for (int i = 0; i < notas.length; i++) {
            if (notas[i] > valor) {
                System.out.print(notas[i] + " ");
            }
        }
    }

    public static boolean isExisteNoArray(int array[], int numero) {

        for (int i = 0; i < array.length; i++) {
            if (array[i] == numero) {
                return true;
            }
        }

        return false;
    }

    public static void inserirNaPosicaoVazia(int[] repetidos, int numero) {

        for (int i = 0; i < repetidos.length; i++) {
            if (repetidos[i] == 0) {
                repetidos[i] = numero;
                break;
            }
        }

    }

    public static int[] buscarRepetidos(int[] numeros) {

        int repetidos[] = new int[numeros.length];

        for (int i = 0; i < numeros.length; i++) {

            for (int j = i; j < numeros.length - 1; j++) {
                if (numeros[i] == numeros[j + 1]) {
                    if (!ArrayUtil.isExisteNoArray(repetidos, numeros[i])) {
                        ArrayUtil.inserirNaPosicaoVazia(repetidos, numeros[i]);
                    }
                }
            }

        }

        return repetidos;

    }

    public static int[] buscarRepetidos(int[][] numeros) {

        int repetidos[] = new int[numeros.length * numeros[0].length];

        for (int i = 0; i < numeros.length; i++) {
            for (int j = 0; j < numeros[i].length - 1; j++) {

                for (int k = i; k < numeros.length; k++) {
                    for (int l = 0; l < numeros[k].length - 1; l++) {
                        
                        if (!isExisteNoArray(repetidos, numeros[k][l])) {
                            inserirNaPosicaoVazia(repetidos, numeros[k][l]);
                        }
                    }
                }

            }
        }

        return repetidos;

    }

    public static int soma(int[] array) {
        int soma = 0 ; 
        
        for (int i = 0; i < array.length; i++) {
            soma+= array[i];
        }
        
        return soma;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

}
