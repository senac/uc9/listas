
public class Ex10 {
    
    public static void main(String[] args) {
        
        int numeros[][] = new int[3][3]; 
        
        ArrayUtil.preencher(numeros);
        
        ArrayUtil.imprimir(numeros);
        
        int repetido[] = ArrayUtil.buscarRepetidos(numeros) ; 
       
        ArrayUtil.imprimir(repetido);
        
        
    }
    
}
