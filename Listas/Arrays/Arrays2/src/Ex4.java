
public class Ex4 {

    public static void main(String[] args) {

        int numeros[] = {1, 5, 6, 7, 9, 10, 82, 3};

        imprimeArray(numeros);
        trocaParImpar(numeros);
        System.out.println("");
        imprimeArray(numeros);
        
    }

    private static void trocaParImpar(int[] numeros) {

        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] % 2 == 0) {
                numeros[i] = 1;
            } else {
                numeros[i] = -1;
            }
        }

    }

    private static void imprimeArray(int[] numeros) {
        for (int i = 0; i < numeros.length; i++) {
            System.out.print(numeros[i] + "\t");
        }

    }

}
