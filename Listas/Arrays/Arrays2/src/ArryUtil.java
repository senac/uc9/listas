
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author sala308b
 */
public class ArryUtil {

    public static void preencherArray(int[] n) {

        for (int i = 0; i < n.length; i++) {
            n[i] = Integer.parseInt(JOptionPane.showInputDialog("Digite Valor : "));
        }
    }
    
    public static void imprimirArray(int[] n ){
         for (int i = 0; i < n.length; i++) {
             System.out.println(n[i] + " , ");
        }
    }

    public static double calcularMedia(double[] notas) {
        return somaElementos(notas) / notas.length;
    }

    public static double somaElementos(double[] notas) {

        double soma = 0;

        for (int i = 0; i < notas.length; i++) {
            soma += notas[i];
        }

        return soma;

    }

    public static void imprimeElementosAcimaValor(double[] notas, double valor) {
        for (int i = 0; i < notas.length; i++) {
            if (notas[i] > valor) {
                System.out.print(notas[i] + " ");
            }
        }
    }

}
