
import javax.swing.JOptionPane;

public class Ex6 {

    public static void main(String[] args) {

        int numeros[] = new int[10];

        preencher(numeros);

        int n = Integer.parseInt(JOptionPane.showInputDialog("Numero"));

        // 1 , 1 , 5 , 9 , 7 
        // 1
        // Existem : 2 ocorrencias.
        int ocorrencias = contaOcorrencias(numeros, n);
        
        
        JOptionPane.showMessageDialog(null, "Existem :" + ocorrencias + " .");

    }
    
    
    private static int contaOcorrencias(int[] numeros, int elementoASerContado) {
        int count = 0;
        
        for (int Elemento : numeros) {
            if(Elemento == elementoASerContado){
                count++;
            }
        }

        return count;
    }

    private static void preencher(int[] n) {
        for (int i = 0; i < n.length; i++) {
            n[i] = Integer.parseInt(JOptionPane.showInputDialog("Numero" + i + ":"));
        }
    }


}
