
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * 5 Escreva um programa que leia as notas de N alunos . Após a leitura das
 * notas calcule a media da turma e a imprima. Imprima também somente as notas
 * acima da media da turma.
 */
public class Ex5 {

    public static void main(String[] args) {

        String aux;
        aux = JOptionPane.showInputDialog("Quantidade de notas ?");

        int tamanho = Integer.parseInt(aux);

        double notas[] = new double[tamanho];

        preencher(notas);

        double media = calcularMedia(notas);

        System.out.println("Media:" + media);

        System.out.print("Notas acima da média: ");

        imprimeElementosAcimaValor(notas, media);

    }

    private static void preencher(double[] notas) {
        for (int i = 0; i < notas.length; i++) {
            notas[i] = Double.parseDouble(JOptionPane.showInputDialog("nota" + i + ":"));
        }
    }

    private static double calcularMedia(double[] notas) {
        return somaElementos(notas) / notas.length;
    }

    private static double somaElementos(double[] notas) {

        double soma = 0;

        for (int i = 0; i < notas.length; i++) {
            soma += notas[i];
        }

        return soma;

    }

    private static void imprimeElementosAcimaValor(double[] notas, double valor) {
        for (int i = 0; i < notas.length; i++) {
            if (notas[i] > valor) {
                System.out.print(notas[i] + " ");
            }
        }
    }

}




















