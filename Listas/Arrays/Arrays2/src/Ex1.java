
import javax.swing.JOptionPane;


public class Ex1 {
    
    public static void main(String[] args) {
        
        int numeros[] = new int[8];
        String aux ; 
        
        for (int i = 0; i < numeros.length; i++) {
            aux = JOptionPane.showInputDialog("Digite numero:");
            numeros[i] =  Integer.parseInt(aux);
        }
        
        imprimirArray(numeros);
        
    }

    private static void imprimirArray(int[] numeros) {
    
        for (int i = 0; i < numeros.length; i++) {
            System.out.print(numeros[i]);
            System.out.print( i != numeros.length-1 ? " , " : "." );
        }
    }
    
}
