
import javax.swing.JOptionPane;

public class Ex2 {

    public static void main(String[] args) {

        int numeros[] = new int[4];
        int numeroBuscado;

        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = Integer.parseInt(JOptionPane.showInputDialog("Digite numero:"));
        }

        numeroBuscado = Integer.parseInt(JOptionPane.showInputDialog("Digite numero a ser buscado:"));

        if (buscaElemento(numeros, numeroBuscado)) {
            JOptionPane.showMessageDialog(null, "Existe !!!!");
        } else {
            JOptionPane.showMessageDialog(null, "Não Existe !!!!");
        }
    }

    private static boolean buscaElemento(int[] numeros, int numeroBuscado) {

        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] == numeroBuscado) {
                return true;
            }
        }
        
        return false;

    }

}
