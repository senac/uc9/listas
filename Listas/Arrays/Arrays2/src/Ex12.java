
public class Ex12 {

    public static void main(String[] args) {

        int matriz[][] = new int[2][2];

        ArrayUtil.preencher(matriz);

        ArrayUtil.imprimir(matriz);

        int menor = matriz[0][0];
        int linha = 0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {

                if (matriz[i][j] < menor) {
                    menor = matriz[i][j];
                    linha = i;
                }
            }
        }

        System.out.println("O Menor elementro esta na linha " + linha);

    }

}
