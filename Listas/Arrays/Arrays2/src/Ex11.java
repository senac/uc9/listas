
public class Ex11 {

    public static void main(String[] args) {

        int matriz[][] = new int[2][3];

        ArrayUtil.preencher(matriz);
        ArrayUtil.imprimir(matriz);

        /* 1 2 3 
           1 5 6
         */
        int somas[] = new int[matriz.length];

        for (int i = 0; i < matriz.length; i++) {
            somas[i] = ArrayUtil.soma(matriz[i]);
        }
        
        System.out.print("Soma das linhas : ");
        ArrayUtil.imprimir(somas);

    }

}
