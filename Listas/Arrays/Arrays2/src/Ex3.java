
import javax.swing.JOptionPane;

public class Ex3 {

    public static void main(String[] args) {

        int[] numeros = new int[3];

        preencherArray(numeros);

        imprimeArray(numeros);

        int posicao = buscarMaior(numeros);

        JOptionPane.showMessageDialog(null, "O Maior elemento "
                + numeros[posicao] + " esta na posicao " + posicao);

    }

    private static void preencherArray(int[] array) {

        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(JOptionPane.showInputDialog("Digite numero:"));
        }
    }

    private static void imprimeArray(int[] numeros) {
        for (int i = 0; i < numeros.length; i++) {
            System.out.print(numeros[i] + " ");
        }

    }

    private static int buscarMaior(int[] array) {

        int posicao = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] > array[posicao]) {
                posicao = i;
            }
        }

        return posicao;
    }

}
