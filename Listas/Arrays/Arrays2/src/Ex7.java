
import javax.swing.JOptionPane;

/*

Crie um array de inteiros de tamanho 10 e os preencha com quaisquer valores
utilizando loop. Após o array estar todo preenchido peça para que o usuário
informe mais um elemento e em qual posição ele que o inserir. Após a inserção
imprimir o array. Lembre‐se que os elementos já existentes no array devem
permanecer .

 */
public class Ex7 {

    // 1 , 2 , 3 
    // 89 , 1 ?
    // 1 , 89 , 2 , 3 --> novo
    public static void main(String[] args) {

        int numeros[] = new int[10];
        int numeroNovo;
        int posicao;

        ArrayUtil.preencher(numeros);

        ArrayUtil.imprimir(numeros);

        numeroNovo = Integer.parseInt(JOptionPane.showInputDialog("Novo Elemento"));

        posicao = Integer.parseInt(JOptionPane.showInputDialog("Posicao"));

        int numerosTemp[] = numeros;

        numeros = new int[numerosTemp.length + 1];

        //copie os elementos 
        for (int i = 0; i < numerosTemp.length; i++) {
            numeros[i] = numerosTemp[i];
        }

        // posicao 2 ---> array 10 
        
        for (int i = numeros.length - 1 ; i > posicao  ; i--) {
            numeros[i] = numeros[i - 1 ] ; 
        }
        
        numeros[posicao] = numeroNovo ; 
        
        ArrayUtil.imprimir(numeros);

    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

}
