
import java.util.Scanner;


public class Ex2 {
    
    //Escreva um algoritmo para ler o salário mensal atual de um funcionário e o percentual de reajuste. 
    //Calcular e escrever o valor do novo salário. 
    
    //100 --> 10
    //100 -- 1 
    //100 * 0.90 -- 90 /100 -> 0.90
    //100 * 1.10 -- > 110 
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        float salario ; 
        float ajuste ; 
        
        System.out.print("Digite Salario:");
        salario = scanner.nextFloat() ; 
        System.out.print("Digite Ajuste:"); // 0.10 10%
        ajuste = scanner.nextFloat() ; 
        float novoSalario = salario + (salario * ajuste) ; 
        System.out.println("Novo Salario:" + novoSalario );
        
        
        
    }
    
    
    
    
    
}
