
import java.text.NumberFormat;
import java.util.Scanner;

/*
9)Ler o salário fixo e o valor das vendas efetuadas pelo vendedor de uma empresa. Sabendo-se que
ele recebe uma comissão de 3% sobre o total das vendas até R$ 1.500,00 mais 5% sobre o que
ultrapassar este valor, calcular e escrever o seu salário total. 

*/
public class Ex9 {

    public static void main(String[] args) {
        
        float salarioBruto ; 
        float valorVendas ; 
        float salarioFinal ; 
        float comissao ; 
        Scanner scanner = new Scanner(System.in) ; 
        
        System.out.print("Digite salario:");
        salarioBruto  = scanner.nextFloat() ; 
        
        System.out.print("Digite Valor de vendas:");
        valorVendas  = scanner.nextFloat() ; 
        
        if(valorVendas <= 1500){
            comissao = valorVendas*0.03f ; 
        }else{
            comissao = 1500 *0.03f ;
            comissao+= (valorVendas - 1500 )*0.05; 
        }
        
        salarioFinal = salarioBruto + comissao ; 
        
        NumberFormat nf = NumberFormat.getCurrencyInstance() ; 
        
        System.out.println("Salario Total:" + nf.format(salarioFinal));
        
        
        

    }
}
