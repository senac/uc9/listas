
import java.util.Scanner;

/*
7)Ler as notas da 1a. e 2a. avaliações de um aluno. Calcular a média aritmética simples e escrever
uma mensagem que diga se o aluno foi ou não aprovado (considerar que nota igual ou maior que 6 o aluno é aprovado). Escrever também a média calculada. 

*/
public class Ex7 {
    public static void main(String[] args) {
        
        float nota1 ; 
        float nota2 ; 
        float media ; 
        
        Scanner scanner = new Scanner(System.in) ; 
        
        System.out.print("Digite Nota1:");
        nota1  = scanner.nextFloat(); 
        
        System.out.print("Digite Nota2:");
        nota2  = scanner.nextFloat(); 
        
        media = (nota1 + nota2 )/2;
        
        if(media >= 6){
            System.out.println("Aluno aprovado!");
        }else{
            System.out.println("Aluno reprovado!");
        }
        
    }
}
