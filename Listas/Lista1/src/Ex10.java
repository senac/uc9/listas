
import java.util.Scanner;

/*
10)Ler 3 valores (considere que não serão informados valores iguais) e escrever o maior deles. 
 */
public class Ex10 {

    public static void main(String[] args) {
        
        int a ; 
        int b ;
        int c ; 
        int maior  ; 
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Digite primeiro valor:");
        a = scanner.nextInt() ; 
        maior = a ; 
        System.out.print("Digite Segundo valor:");
        b = scanner.nextInt() ; 
        if(b > maior){
            maior = b; 
        }
        System.out.print("Digite Terceiro valor:");
        c = scanner.nextInt() ; 
        if(c > maior){
            maior = c ; 
        }
        System.out.print("Maior valor é:" + maior );
        

    }
}
