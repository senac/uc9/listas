
import java.util.Scanner;

/*

4)Uma revendedora de carros usados paga a seus funcionários vendedores um salário fixo por mês,
mais uma comissão também fixa para cada carro vendido e mais 5% do valor das vendas por ele
efetuadas. Escrever um algoritmo que leia o número de carros por ele vendidos, o valor total de suas
vendas, o salário fixo e o valor que ele recebe por carro vendido. Calcule e escreva o salário final do
vendedor. 



 */
public class Ex4 {

    public static void main(String[] args) {

        float salarioFixo;
        int quantidadeVendida;
        float valorTotalVendas;
        float valorPorCarro;
        final float comissao = 0.05f;
        float salarioFinal = 0 ; 

        Scanner scanner = new Scanner(System.in);

        System.out.print("Digite Quantidade de Carros Vendidos:");
        quantidadeVendida = scanner.nextInt();
        
        System.out.print("Digite Valor Total de vendas:");
        valorTotalVendas = scanner.nextFloat();
        
        System.out.print("Digite Salario Fixo:");
        salarioFixo = scanner.nextFloat();
        
        System.out.print("Digite valor por carro vendido:");
        valorPorCarro = scanner.nextFloat();
        
        //salariofixo 
        salarioFinal+= salarioFixo ; 
        
        //Comissao vendas
        salarioFinal+= valorTotalVendas * comissao ; 
        
        //valor por carro 
        salarioFinal+= valorPorCarro * quantidadeVendida ; 
        
        
        System.out.println("Salario Final:" + salarioFinal);
        
        

    }
}
