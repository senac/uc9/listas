
import java.util.Scanner;

/*
8)Ler dois valores (considere que não serão lidos valores iguais) e escrevê-los em ordem crescente. 
*/
public class Ex8 {
    public static void main(String[] args) {
        
        int a ; 
        int b ; 
        
        Scanner scanner = new Scanner(System.in) ; 
        
        System.out.print("Digite Valor:");
        a  = scanner.nextInt() ; 
        
        System.out.print("Digite Valor:");
        b  = scanner.nextInt() ; 
        
        if(a < b){
            System.out.println( a + " " + b);
        }else{
            System.out.println( b + " " + a);
        }
        
        
    }
}
