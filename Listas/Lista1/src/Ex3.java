
import java.text.NumberFormat;
import java.util.Scanner;

/*
3)O custo de um carro novo ao consumidor é a soma do custo de fábrica com a porcentagem do
distribuidor e dos impostos (aplicados ao custo de fábrica). 
Supondo que o percentual do distribuidor seja de 28% e os impostos de 45%, 
escrever um algoritmo para ler o custo de fábrica de um carro, calcular e escrever o custo final ao consumidor. 

*/
public class Ex3 {
    
    public static void main(String[] args) {
        
         float custoFabrica;
        final float percentualFabrica = 0.28f;
        final float impostos = 0.45f;
        float custoFinalComsumidor  = 0 ; 
        

        Scanner scanner = new Scanner(System.in);

        System.out.print("Digite Custo de Fabrica do Veiculo:");
        custoFabrica = scanner.nextFloat() ; 
        
        //soma do custo de fábrica com a porcentagem do distribuidor e dos impostos (aplicados ao custo de fábrica).
        custoFinalComsumidor = custoFabrica + ( custoFabrica * percentualFabrica) + (custoFabrica * impostos) ; 
        NumberFormat nf = NumberFormat.getCurrencyInstance() ; 
        System.out.println("Custo Consumidor:" + nf.format(custoFinalComsumidor) );
        
    }
    
}
