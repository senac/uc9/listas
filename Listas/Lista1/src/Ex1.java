
import java.util.Scanner;




public class Ex1 {
    
    //Escreva um algoritmo para ler um valor (do teclado) e escrever (na tela) o seu antecessor
    
    public static void main(String[] args) {
        //Leitor  
        int numero ; 
        Scanner teclado = new Scanner(System.in) ; 
        System.out.print("Digite numero:");
        numero = teclado.nextInt();
        System.out.println("Numero anterior a " + numero + " e " + (numero-1) );
        
        
    }
    
}
