
import java.util.Scanner;

/*
5)Ler um valor e escrever a mensagem É MAIOR QUE 10! se o valor lido for maior que 10, caso
contrário escrever NÃO É MAIOR QUE 10! 

*/
public class Ex5 {

    public static void main(String[] args) {
        
        int valor;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Digite valor:");
        valor = scanner.nextInt();

        if (valor < 10) {
            System.out.println("NÃO É MAIOR QUE 10!");
        }else{
            System.out.println(" MAIOR QUE 10!");
        }

    }
}
