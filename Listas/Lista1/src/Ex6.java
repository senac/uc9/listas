
import java.util.Scanner;

/*
6)As maçãs custam R$ 1,30 cada se forem compradas menos de uma dúzia, e R$ 1,00 se forem
compradas pelo menos 12. Escreva um programa que leia o número de maçãs compradas, calcule e
escreva o custo total da compra. 

*/
public class Ex6 {
    public static void main(String[] args) {
        final float valorUnitario = 1.3f ; 
        final float valorDuzia = 1.0f ; 
        int quantidadeComprada ; 
        float totalCompra  ; 
        
        Scanner scanner = new Scanner(System.in) ; 
        
        System.out.print("Digite Quantidade de Maças compradas:");
        quantidadeComprada  = scanner.nextInt() ; 
        
        if(quantidadeComprada <12 ){
            totalCompra = quantidadeComprada * valorUnitario;
        }else{
            totalCompra = quantidadeComprada * valorDuzia ; 
        }
        
        System.out.println("Valor total da compra:" + totalCompra);
    }
}
