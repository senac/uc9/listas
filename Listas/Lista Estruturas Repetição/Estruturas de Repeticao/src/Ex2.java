
import java.util.Scanner;


public class Ex2 {
    
    public static void main(String[] args) {
        
        int numero   ; 
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite o numero da tauaba:");
        numero = scanner.nextInt() ; 
        
        for (int i = 1; i <= 10; i++) {
            
            String linha = String.format("%d x %d = %d", numero ,i , (numero * i) ) ; 
            System.out.println(linha);
        }
                
       
    }
    
}
