
import javax.swing.JOptionPane;

public class Ex10 {

    public static void main(String[] args) {

        int numeroInscricao;
        int idade;
        char sexo;
        char experienciaServico;

        // Variaveis de contagem 
        int contaMasculino = 0;
        int contaFeminino = 0;

        int somaTotalIdadeExperiencia = 0;
        int contaTotalIdadeExperiencia = 0;
        double MediaMasculinoIdadeExperiencia = 0;
        int contaQuarentaECinco = 0;
        double percentualMais45 = 0;
        int contaIdade35ComServico = 0;
        int menorIdade = 0;

        while (true) {

            try {
                numeroInscricao = Integer.parseInt(JOptionPane.showInputDialog("Numeor Inscricao:"));
                idade = Integer.parseInt(JOptionPane.showInputDialog("Idade:"));
                sexo = JOptionPane.showInputDialog("Sexo(M/F):").toUpperCase().charAt(0);
                experienciaServico = JOptionPane.
                        showInputDialog("Possui Experiencia ? (S/N):").toUpperCase().charAt(0);

                switch (sexo) {
                    case 'M':
                        contaMasculino++;

                        if (experienciaServico == 'S') {
                            somaTotalIdadeExperiencia += idade;
                            contaTotalIdadeExperiencia++;
                        }

                        if (idade > 45) {
                            contaQuarentaECinco++;
                        }

                        break;
                    case 'F':
                        contaFeminino++;

                        if (experienciaServico == 'S') {

                            if (idade < 35) {
                                contaIdade35ComServico++;
                            }

                            if (menorIdade == 0 || idade < menorIdade) {
                                menorIdade = idade;
                            }
                        }

                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Sexo Invalido!!!");
                }
            } catch (NumberFormatException ex) {

                JOptionPane.showMessageDialog(null, "Computando dados....");
                break;

            }

        }

        MediaMasculinoIdadeExperiencia = somaTotalIdadeExperiencia / contaTotalIdadeExperiencia;
        percentualMais45 = contaQuarentaECinco / contaMasculino;

        JOptionPane.showMessageDialog(null, 
                "Candidatos Feminino:" + contaFeminino + "\n" + 
                "Candidatos Masculino:" + contaMasculino + "\n" +
                "Candidatos Media Masculino com experiencia:" + MediaMasculinoIdadeExperiencia + "\n" +
                "Candidatos % Masculino acima de 45 anos:" + contaIdade35ComServico + "\n" +
                "Candidatos Feminino menos 35 com experiencia:" + contaIdade35ComServico + "\n" +
                "Candidatos Idade feminino:" + menorIdade );

    }
}
