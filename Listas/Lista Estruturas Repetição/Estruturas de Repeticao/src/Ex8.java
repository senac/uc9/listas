
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * 8. Elaborar um algoritmo que apresente os resultados da soma e da média
 * aritmética dos valores pares situados na faixa numérica de 50 a 70.
 */
public class Ex8 {

    public static void main(String[] args) {

        double soma = 0;
        double media = 0;
        int quantidade = 0;

        for (int i = 50; i <= 70; i++) {
            if (i % 2 == 0) {
                soma += i;
                quantidade++;
            }
        }

        media = soma / quantidade;

        JOptionPane.showMessageDialog(null, "Soma:" + soma + "\n" + "Media:" + media);

    }

}
