
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * 7. Elaborar um algoritmo que efetue a leitura sucessiva de valores numéricos
 * e apresente no final o total do somatório, a média e o total de valores
 * lidos. O algoritmo deve fazer as leituras dos valores enquanto o usuário
 * estiver fornecendo valores positivos. Ou seja, o algoritmo deve parar quando
 * o usuário fornecer um valor menor ou igual à zero.
 *
 */
public class Ex7 {

    public static void main(String[] args) {
        int numero = 0;
        int soma = 0;
        double media = 0;
        int quantidadeLeituras = 0;

        while (true) {
            numero = Integer.parseInt(JOptionPane.showInputDialog("Numero?"));
            
            if (numero <= 0) {
                break;
            }

            soma += numero;
            quantidadeLeituras++;

        };

        media = soma / quantidadeLeituras;

        JOptionPane.showMessageDialog(null,
                "Somatorio:" + soma
                + "\n"
                + "Quantidade de valores:" + quantidadeLeituras
                + "\n"
                + "Media:" + media);

        
        
    }

}
