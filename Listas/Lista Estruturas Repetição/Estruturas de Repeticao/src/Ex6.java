
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * 6. Elaborar um algoritmo que efetue a leitura de 10 valores numéricos e
 * apresente no final o total do somatório e a média dos valores lidos.
 */
public class Ex6 {

    public static void main(String[] args) {

        int soma = 0;
        double media = 0;
        final int leituras = 10;

        for (int i = 0; i < leituras; i++) {
            int numero;
            numero = Integer.parseInt(JOptionPane.showInputDialog("Numero?"));
            soma += numero;
        }

        media = soma / leituras;

        JOptionPane.showMessageDialog(null, "Somatorio:" + soma + "\n" + "Media:" + media);

    }

}
