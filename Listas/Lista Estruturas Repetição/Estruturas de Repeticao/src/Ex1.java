
public class Ex1 {
    /*
    1. Desenvolva um algoritmo que imprima o quadrado
    dos números inteiros de 15 a 200.    
    */
    
    
    public static void main(String[] args) {
        
        
        for (int i = 15; i <= 200; i++) {
            System.out.println("Quadrado de " + i +" "+( i * i) );
        }
        
        
        
    }
}
