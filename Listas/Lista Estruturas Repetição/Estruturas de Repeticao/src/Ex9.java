
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * 9. Elaborar um algoritmo que efetue a leitura de valores positivos inteiros
 * até que um valor negativo seja informado. Ao final devem ser escritos o maior
 * e o menor valor informados.
 */
public class Ex9 {

    public static void main(String[] args) {

        int maior;
        int menor;
        int numero;

        // 1º leitura nao sei quem é o maior ou menor 
        numero = Integer.parseInt(JOptionPane.showInputDialog("Numero?"));
        maior = numero;
        menor = numero;

        while (true) {
           
            numero = Integer.parseInt(JOptionPane.showInputDialog("Numero?"));

            if (numero < 0) {
                break;
            }

            if (numero > maior) {
                maior = numero;
            } else if (numero < menor) {
                menor = numero;
            }

        }

        JOptionPane.showMessageDialog(null,
                "Maior:" + maior
                + "\n"
                + "Menor:" + menor);

    }

}
