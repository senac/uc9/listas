
import javax.swing.JOptionPane;

/**
 * 8. Faça um programa que receba três números obrigatoriamente em ordem
 * crescente e um quarto número que não siga essa regra. Mostre, em seguida, os
 * quatro números em ordem decrescente. Suponha que o usuário digitará quatro
 * números diferentes.
 */
public class Ex8 {

    public static void main(String[] args) {

        // 1, 3 , 5  --> Ordem crescente
        int numero1;
        int numero2;
        int numero3;
        int numeroX;

        String aux;

        aux = JOptionPane.showInputDialog("Digite numero1:");
        numero1 = Integer.parseInt(aux);

        aux = JOptionPane.showInputDialog("Digite numero2:");
        numero2 = Integer.parseInt(aux);

        aux = JOptionPane.showInputDialog("Digite numero3:");
        numero3 = Integer.parseInt(aux);

        aux = JOptionPane.showInputDialog("Digite X:");
        numeroX = Integer.parseInt(aux);

        /*
        String resposta = ""; //"Numeros:" + numero3 + numero2 + numero1

        if (numeroX > numero3) {
            resposta = String.valueOf(numeroX) + " , ";
        }

        resposta += String.valueOf(numero3) + " , ";

        if (numeroX > numero2 && numeroX < numero3) {

            resposta += String.valueOf(numeroX) + " , ";
        }

        resposta += String.valueOf(numero2) + " , ";

        if (numeroX > numero1 && numeroX < numero2) {
            resposta += String.valueOf(numeroX);
        }

        resposta += String.valueOf(numero1) + " , ";

        if (numeroX < numero1) {
            resposta += String.valueOf(numeroX) + " , ";
        }
        
         */
        StringBuffer sb = new StringBuffer();

        if (numeroX > numero3) {
            sb.append(numeroX);
        }

        sb.append(numero3);

        if (numeroX > numero2 && numeroX < numero3) {

            sb.append(numeroX);
        }

        sb.append(numero2);

        if (numeroX > numero1 && numeroX < numero2) {
            sb.append(numeroX);
        }

        sb.append(numero1);

        if (numeroX < numero1) {
            sb.append(numeroX);
        }

        
        JOptionPane.showMessageDialog(null, sb);
        
        System.err.println(sb.reverse());


        /*
        if (numeroX > numero3) {
            System.out.println(numeroX);
        }

        System.out.println(numero3);
        if (numeroX > numero2 && numeroX < numero3) {
            System.out.println(numeroX);
        }

        System.out.println(numero2);

        if (numeroX > numero1 && numeroX < numero2) {
            System.out.println(numeroX);
        }

        System.out.println(numero1 );

        if (numeroX < numero1) {
            System.out.println(numeroX);
        }

         */
        //JOptionPane.showMessageDialog(null, resposta );
        // ler numero 4 
        //  ---> 5 , 4 , 3 , 1 
    }

}
