
import javax.swing.JOptionPane;

public class Ex7 {

    public static void main(String[] args) {

        double nota1;
        double nota2;
        double nota3;
        double media;

        String aux = null;

        try {

            aux = JOptionPane.showInputDialog("Digite nota1:");
            nota1 = Double.parseDouble(aux);

            aux = JOptionPane.showInputDialog("Digite nota2:");
            nota2 = Double.parseDouble(aux);

            aux = JOptionPane.showInputDialog("Digite nota3:");
            nota3 = Double.parseDouble(aux);

            media = calculaMedia(nota1, nota2, nota3);

            JOptionPane.showMessageDialog(null,
                    getConceito(media)
                    + "\n"
                    + "Media:" + media);

            //qual conceito ???
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(null,
                    "Erro na conversao", "Erro",
                    JOptionPane.ERROR_MESSAGE);
        }

    }

    public static String getConceito(double media) {

        if (media >= 8) {
            return "Conceito A";
        } else if (media >= 7) {
            return "Conceito B";
        } else if (media >= 6) {
            return "Conceito C";
        } else if (media >= 5) {
            return "Conceito D";
        } else {
            return "Conceito E ";
        }

    }

    private static double calculaMedia(double nota1, double nota2, double nota3) {
        return (nota1 + nota2 + nota3) / 3;
    }

}
