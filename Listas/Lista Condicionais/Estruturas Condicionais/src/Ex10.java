
import javax.swing.JOptionPane;

public class Ex10 {

    public static void main(String[] args) {

        do {

            double bonificacao;
            double auxilioEscola;
            double salario = 0;
            double salarioReajustado;
            String aux;

            try {

                aux = JOptionPane.showInputDialog("Digite o salario do funcionario");

                if (aux == null) {
                    System.exit(0);
                }

                salario = Double.parseDouble(aux);

                //bonificacao 
                //   bonificacao = calculaBonificacao(salario);
                // auxilioEscola = calculaAuxilioEscola(salario);
                //auxilio Escola
                // salarioReajustado = salario +  bonificacao + auxilioEscola;
                salarioReajustado = calcularNovoSalario(salario);

                JOptionPane.showMessageDialog(null,
                        "Salario atual:" + salario
                        + "\n"
                        + "Salario Reajustado:" + salarioReajustado);

            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Erro ao converter", "erro", JOptionPane.ERROR_MESSAGE);
            }

        } while (true);

    }

    private static double calculaBonificacao(double salario) {
        if (salario <= 500) {
            return salario * 0.05;
        } else if (salario <= 1200) {
            return salario * 0.12;
        } else {
            return salario * 0;
        }
    }

    private static double calculaAuxilioEscola(double salario) {
        if (salario <= 600) {
            return 150;
        } else {
            return 100;
        }
    }

    private static double calcularNovoSalario(double salario) {
        return salario + calculaBonificacao(salario) + calculaAuxilioEscola(salario);
    }

}
