
import javax.swing.JOptionPane;

/**
 *
 * 6. Faça um programa que receba um número inteiro e verifique se é par ou
 * ímpar.
 */
public class Ex6 {

    public static void main(String[] args) {

        int numero;

        String aux = null;

        aux = JOptionPane.showInputDialog(null, "Digite o numero ");

        numero = Integer.parseInt(aux);

        verificarParImpar(numero);

    }

    public static void verificarParImpar(int numero) {

        if (numero % 2 == 0) {
            JOptionPane.showMessageDialog(null, "Par");
        } else {
            JOptionPane.showMessageDialog(null, "Impar");
        }

    }

    public static boolean verificarPar(int numero) {
        return numero % 2 == 0;
    }

    public static boolean verificarImpar(int numero) {
        return numero % 2 != 0;
    }

}
