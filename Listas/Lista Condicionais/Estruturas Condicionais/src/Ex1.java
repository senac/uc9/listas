
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * 1. Fazer um programa para ler duas medidas de um retângulo. O programa deve
 * mostrar o perímetro e a área do retângulo a partir das medidas recebidas.
 */
public class Ex1 {

    public static void main(String[] args) {

        float altura;
        float largura;
        float perimetro;

        Scanner scanner = new Scanner(System.in);
        System.out.print("Digite a altura :");
        altura = scanner.nextFloat();
        System.out.print("Digite a largura :");
        largura = scanner.nextFloat();
        perimetro = calcularPerimetro(altura, largura);
        System.out.println(String.format("Perimetro: %f", perimetro));

    }

    private static float calcularPerimetro(float altura, float largura) {
        return (2 * altura) + (2 * largura);
    }

}
