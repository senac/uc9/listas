
import javax.swing.JOptionPane;

/**
 *
 * 5. Fazer um programa para ler dois números inteiros e mostre qual deles é o
 * maior.
 */
public class Ex5 {

    public static void main(String[] args) {

        int numero1;
        int numero2;

        String aux = null;

        aux = JOptionPane.showInputDialog(null, "Digite o numero ");

        numero1 = Integer.parseInt(aux);

        aux = JOptionPane.showInputDialog(null, "Digite o numero ");

        numero2 = Integer.parseInt(aux);

        JOptionPane.showMessageDialog(null, "O numero maior é :" + maior(numero1, numero2));

    }

    
    public static int maior(int n1, int n2) {

        return n1 > n2 ? n1 : n2;

        /*
        int numero = 0;
        if (n1 > n2) {
            numero = n1;
        } else {
            numero = n2;
        }

        return numero;
         */
    }

   
    
    
}
