
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * 2. Fazer um programa para ler os valores de quatro números N1, N2, N3 e N4.
    Calcule e imprima o valor da média aritmética dos mesmos.
 */
public class Ex2 {
    public static void main(String[] args) {

        double n1;
        double n2;
        double n3;
        double n4;
        double media;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Digite a n1 :");
        n1 = scanner.nextDouble();
        System.out.print("Digite a n2 :");
        n2 = scanner.nextDouble();
        System.out.print("Digite a n3 :");
        n3 = scanner.nextDouble();
        System.out.print("Digite a n4 :");
        n4 = scanner.nextDouble();
        media = (n1 + n2 + n3 + n4 ) / 4 ; 
        
         System.out.println("Media:" +  media);
       

    }
}
