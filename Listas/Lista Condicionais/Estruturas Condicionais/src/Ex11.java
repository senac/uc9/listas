
import javax.swing.JOptionPane;

public class Ex11 {

    public static void main(String[] args) {

        double lado1;
        double lado2;
        double lado3;
        double valor;
        String aux;

        aux = JOptionPane.showInputDialog("Digite lado 1:");
        lado1 = Double.parseDouble(aux);

        aux = JOptionPane.showInputDialog("Digite lado 2:");
        lado2 = Double.parseDouble(aux);

        aux = JOptionPane.showInputDialog("Digite lado 3:");
        lado3 = Double.parseDouble(aux);

        if (existeTriangulo(lado1, lado2, lado3)) {
            classificaTriangulo(lado1, lado2, lado3);
        } else {
            JOptionPane.showMessageDialog(null, "Não e triangulo", "Erro", JOptionPane.ERROR_MESSAGE);
        }

    }

    private static boolean existeTriangulo(double lado1, double lado2, double lado3) {

        return ((lado1 < lado2 + lado3) && (lado2 < lado1 + lado3) && (lado3 < lado1 + lado2));

    }

    private static void classificaTriangulo(double lado1, double lado2, double lado3) {

        JOptionPane.showMessageDialog(null, "Eh um triangulo!!!");

        if ((lado1 == lado2) && lado2 == lado3) {
            JOptionPane.showMessageDialog(null, "Triangulo Equilatero");
        } else if ((lado1 == lado2) || lado2 == lado3 || lado1 == lado3) {
            JOptionPane.showMessageDialog(null, "Triangulo Isoceles");
        } else {
            JOptionPane.showMessageDialog(null, "Triangulo Escaleno");
        }

    }

}
