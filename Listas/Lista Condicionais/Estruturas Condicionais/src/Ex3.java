
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * 3. Fazer um programa para ler um valor numérico qualquer x e dois valores a e
 * b tais que a < b, teste se x está no intervalo fechado [a,b].
 */
public class Ex3 {

    public static void main(String[] args) {

        int x;
        int a;
        int b;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Digite a X :");
        x = scanner.nextInt();
        System.out.print("Digite o Valor A do intervalo :");
        a = scanner.nextInt();
        System.out.print("Digite o Valor B do intervalo :");
        b = scanner.nextInt();

        if (x >= a && x <= b) {
            System.out.println(String.format("%d esta no intervalo fechado [%d , %d].", x, a, b));
        } else {
            System.out.println(String.format("%d não esta no intervalo fechado [%d , %d].", x, a, b));
        }

    }
}
