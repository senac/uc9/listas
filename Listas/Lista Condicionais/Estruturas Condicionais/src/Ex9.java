
import java.awt.SystemColor;
import javax.swing.JOptionPane;

/**
 *
 * Faça um programa que mostre o menu de opções (1 e 2) a seguir, receba a opção
 * do usuário e os dados necessários para executar cada operação. 1 – Somar dois
 * números; 2 – Raiz quadrada de um número;
 */
public class Ex9 {

    public static void main(String[] args) {

        double resultado = 0 ; 
        double numero1;
        double numero2;
        String aux;

        do {
            try {
                switch (menu()) {
                    case 1:

                        aux = JOptionPane.showInputDialog("Digite numero1:");
                        numero1 = Double.parseDouble(aux);

                        aux = JOptionPane.showInputDialog("Digite numero2:");
                        numero2 = Double.parseDouble(aux);

                        resultado = soma(numero1, numero2);

                        break;
                    case 2:

                        aux = JOptionPane.showInputDialog("Digite numero1:");
                        numero1 = Double.parseDouble(aux);

                        resultado = raiz(numero1);
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Opcao Invalida!!!");
                }

                JOptionPane.showMessageDialog(null, "Resultado:" + resultado);

            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Falha na conversao");
            }

        } while (true);

    }

    private static int menu() {

        int opcao = 0;

        String aux;
        aux = JOptionPane.showInputDialog(
                "#####Calculadora#####"
                + "\n"
                + "1 – Somar dois números"
                + "\n 2 – Raiz quadrada de um número");

        opcao = Integer.parseInt(aux);

        return opcao;
    }

    private static double soma(double numero1, double numero2) {
        return numero1 + numero2;
    }

    private static double raiz(double numero1) {
        return Math.sqrt(numero1);
    }

}
