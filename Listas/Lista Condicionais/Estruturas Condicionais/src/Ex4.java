
import javax.swing.JOptionPane;

/*
Fazer um programa para ler um número e mostrar na tela uma mensagem
dizendo se ele é positivo, negativo ou nulo.
 */
public class Ex4 {

    public static void main(String[] args) {

        int numero;
        String aux = null;

        try {
            aux = JOptionPane.showInputDialog("Digite o numero:");
            numero = Integer.parseInt(aux);
            JOptionPane.showMessageDialog(null, verificaValor(numero));
        } catch (NumberFormatException ex) {
            String mensagem = "Nao foi possivel converter: " + aux;
            JOptionPane.showMessageDialog(null, mensagem, "Erro", JOptionPane.INFORMATION_MESSAGE);
        }

    }

    public static String verificaValor(int numero) {

        return numero == 0  ?  "Nulo" : numero > 0 ? "Positivo"  :  "Negativo"  ;  
        
        /*
        if (numero == 0) {
            return "Numero nulo";
        } else if (numero > 0) {
            return "Numero Positivo";
        } else {
            return "Numero Negativo";
        }
*/

    }

}
